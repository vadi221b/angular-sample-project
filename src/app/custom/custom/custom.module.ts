import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardContentComponent } from './card/card-content/card-content.component';
import { CardHeaderComponent } from './card/card-content/card-header/card-header.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CardContentComponent, CardHeaderComponent]
})
export class CustomModule { }